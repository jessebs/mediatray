#pragma once
#include "MediaTrayBroker.h"
#include <Windows.h>

namespace gen_mediatray {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for Display
	/// </summary>
	public ref class Display : public System::Windows::Forms::Form
	{
	public:
		Display(MediaTrayBroker* broker)
		{
			this->broker = broker;
			InitializeComponent();
			lastOver = 0;
			//
			//TODO: Add the constructor code here
			//
			positionForm();
			roundForm();
		}

		void ShowActions() {
			Opacity = 1.0;
			lastOver = 0;
			Focus();
			updateWinampData();
			Show();
		}

		void quit() {
			Visible=false;
			trayIcon->Visible = false;
		}

		void updateWinampData(){

			this->titleLabel->Text = broker->getTitle();
			this->artistLabel->Text = broker->getArtist();

			this->trayIcon->Text = this->artistLabel->Text + L"\r\n" + this->titleLabel->Text;
			
			if(broker->isPlaying()){
				this->playpausePicture->Image = this->pausePicture->Image;
			} else {
				this->playpausePicture->Image = this->playPicture->Image;
			}
			Refresh();
		}


	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Display()
		{
			quit();
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Label^  titleLabel;
	private: System::Windows::Forms::PictureBox^  backPicture;
	private: System::Windows::Forms::PictureBox^  nextPicture;
	private: System::Windows::Forms::PictureBox^  playpausePicture;
	private: System::Windows::Forms::PictureBox^  playPicture;
	private: System::Windows::Forms::PictureBox^  pausePicture;
	private: System::Windows::Forms::Label^  artistLabel;
	private: System::Windows::Forms::ContextMenuStrip^  contextMenu;
	private: System::Windows::Forms::ToolStripMenuItem^  showWinampToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  exitWinampToolStripMenuItem;
	private: System::Windows::Forms::Timer^  vanishingTimer;



	protected: 

	protected: 
	private: 
		System::Windows::Forms::NotifyIcon^  trayIcon;
		static const double VANISH_DECREMENT = 0.05;
		int lastOver;
	private: System::Windows::Forms::Timer^  mouseTimer;
		static const int MAX_SHOW_TIME = 3000;
		static bool overSubcomponent = false;

		void positionForm() {
			StartPosition = FormStartPosition::Manual;
			int areaWidth = Screen::PrimaryScreen->WorkingArea.Width;
			int areaHeight = Screen::PrimaryScreen->WorkingArea.Height;
			Location = Point(areaWidth - Width, areaHeight - Height);
		}

		void roundForm() {
			this->Region = System::Drawing::Region::FromHrgn((IntPtr)CreateRoundRectRgn(1, 1, this->Width, this->Height, 16, 16));
		}

	private: 
		MediaTrayBroker* broker;

	protected: 
		
	protected: 

	protected: 

	protected: 
	private: System::ComponentModel::IContainer^  components;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(Display::typeid));
			this->trayIcon = (gcnew System::Windows::Forms::NotifyIcon(this->components));
			this->contextMenu = (gcnew System::Windows::Forms::ContextMenuStrip(this->components));
			this->showWinampToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->exitWinampToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->titleLabel = (gcnew System::Windows::Forms::Label());
			this->backPicture = (gcnew System::Windows::Forms::PictureBox());
			this->nextPicture = (gcnew System::Windows::Forms::PictureBox());
			this->playpausePicture = (gcnew System::Windows::Forms::PictureBox());
			this->playPicture = (gcnew System::Windows::Forms::PictureBox());
			this->pausePicture = (gcnew System::Windows::Forms::PictureBox());
			this->artistLabel = (gcnew System::Windows::Forms::Label());
			this->vanishingTimer = (gcnew System::Windows::Forms::Timer(this->components));
			this->mouseTimer = (gcnew System::Windows::Forms::Timer(this->components));
			this->contextMenu->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->backPicture))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->nextPicture))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->playpausePicture))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->playPicture))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pausePicture))->BeginInit();
			this->SuspendLayout();
			// 
			// trayIcon
			// 
			this->trayIcon->ContextMenuStrip = this->contextMenu;
			this->trayIcon->Icon = (cli::safe_cast<System::Drawing::Icon^  >(resources->GetObject(L"trayIcon.Icon")));
			this->trayIcon->Text = L"MediaTray";
			this->trayIcon->Visible = true;
			this->trayIcon->MouseClick += gcnew System::Windows::Forms::MouseEventHandler(this, &Display::trayIcon_MouseClick);
			// 
			// contextMenu
			// 
			this->contextMenu->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {this->showWinampToolStripMenuItem, 
				this->exitWinampToolStripMenuItem});
			this->contextMenu->Name = L"contextMenu";
			this->contextMenu->Size = System::Drawing::Size(152, 48);
			// 
			// showWinampToolStripMenuItem
			// 
			this->showWinampToolStripMenuItem->Name = L"showWinampToolStripMenuItem";
			this->showWinampToolStripMenuItem->Size = System::Drawing::Size(151, 22);
			this->showWinampToolStripMenuItem->Text = L"Show Winamp";
			this->showWinampToolStripMenuItem->Click += gcnew System::EventHandler(this, &Display::showWinampToolStripMenuItem_Click);
			// 
			// exitWinampToolStripMenuItem
			// 
			this->exitWinampToolStripMenuItem->Name = L"exitWinampToolStripMenuItem";
			this->exitWinampToolStripMenuItem->Size = System::Drawing::Size(151, 22);
			this->exitWinampToolStripMenuItem->Text = L"Exit Winamp";
			this->exitWinampToolStripMenuItem->Click += gcnew System::EventHandler(this, &Display::exitWinampToolStripMenuItem_Click);
			// 
			// titleLabel
			// 
			this->titleLabel->Font = (gcnew System::Drawing::Font(L"Arial Rounded MT Bold", 8.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->titleLabel->Location = System::Drawing::Point(10, 191);
			this->titleLabel->Name = L"titleLabel";
			this->titleLabel->Size = System::Drawing::Size(260, 23);
			this->titleLabel->TabIndex = 0;
			this->titleLabel->Text = L"<Title>";
			this->titleLabel->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			this->titleLabel->MouseEnter += gcnew System::EventHandler(this, &Display::titleLabel_MouseEnter);
			this->titleLabel->MouseLeave += gcnew System::EventHandler(this, &Display::titleLabel_MouseLeave);
			// 
			// backPicture
			// 
			this->backPicture->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"backPicture.Image")));
			this->backPicture->Location = System::Drawing::Point(12, 217);
			this->backPicture->Name = L"backPicture";
			this->backPicture->Size = System::Drawing::Size(33, 33);
			this->backPicture->TabIndex = 1;
			this->backPicture->TabStop = false;
			this->backPicture->MouseClick += gcnew System::Windows::Forms::MouseEventHandler(this, &Display::backPicture_MouseClick);
			this->backPicture->MouseEnter += gcnew System::EventHandler(this, &Display::backPicture_MouseEnter);
			this->backPicture->MouseLeave += gcnew System::EventHandler(this, &Display::backPicture_MouseLeave);
			// 
			// nextPicture
			// 
			this->nextPicture->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"nextPicture.Image")));
			this->nextPicture->Location = System::Drawing::Point(237, 217);
			this->nextPicture->Name = L"nextPicture";
			this->nextPicture->Size = System::Drawing::Size(33, 33);
			this->nextPicture->TabIndex = 2;
			this->nextPicture->TabStop = false;
			this->nextPicture->MouseClick += gcnew System::Windows::Forms::MouseEventHandler(this, &Display::nextPicture_MouseClick);
			this->nextPicture->MouseEnter += gcnew System::EventHandler(this, &Display::nextPicture_MouseEnter);
			this->nextPicture->MouseLeave += gcnew System::EventHandler(this, &Display::nextPicture_MouseLeave);
			// 
			// playpausePicture
			// 
			this->playpausePicture->Image = this->playPicture->Image;
			this->playpausePicture->Location = System::Drawing::Point(122, 217);
			this->playpausePicture->Name = L"playpausePicture";
			this->playpausePicture->Size = System::Drawing::Size(33, 33);
			this->playpausePicture->TabIndex = 3;
			this->playpausePicture->TabStop = false;
			this->playpausePicture->MouseClick += gcnew System::Windows::Forms::MouseEventHandler(this, &Display::playpausePicture_MouseClick);
			this->playpausePicture->MouseEnter += gcnew System::EventHandler(this, &Display::playpausePicture_MouseEnter);
			this->playpausePicture->MouseLeave += gcnew System::EventHandler(this, &Display::playpausePicture_MouseLeave);
			// 
			// playPicture
			// 
			this->playPicture->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"playPicture.Image")));
			this->playPicture->Location = System::Drawing::Point(237, 45);
			this->playPicture->Name = L"playPicture";
			this->playPicture->Size = System::Drawing::Size(33, 33);
			this->playPicture->TabIndex = 4;
			this->playPicture->TabStop = false;
			this->playPicture->Visible = false;
			this->playPicture->MouseEnter += gcnew System::EventHandler(this, &Display::playPicture_MouseEnter);
			this->playPicture->MouseLeave += gcnew System::EventHandler(this, &Display::playPicture_MouseLeave);
			// 
			// pausePicture
			// 
			this->pausePicture->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"pausePicture.Image")));
			this->pausePicture->Location = System::Drawing::Point(198, 45);
			this->pausePicture->Name = L"pausePicture";
			this->pausePicture->Size = System::Drawing::Size(33, 33);
			this->pausePicture->TabIndex = 5;
			this->pausePicture->TabStop = false;
			this->pausePicture->Visible = false;
			this->pausePicture->MouseEnter += gcnew System::EventHandler(this, &Display::pausePicture_MouseEnter);
			this->pausePicture->MouseLeave += gcnew System::EventHandler(this, &Display::pausePicture_MouseLeave);
			// 
			// artistLabel
			// 
			this->artistLabel->Font = (gcnew System::Drawing::Font(L"Arial Rounded MT Bold", 8.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->artistLabel->Location = System::Drawing::Point(10, 168);
			this->artistLabel->Name = L"artistLabel";
			this->artistLabel->Size = System::Drawing::Size(260, 23);
			this->artistLabel->TabIndex = 6;
			this->artistLabel->Text = L"<Artist>";
			this->artistLabel->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			this->artistLabel->MouseEnter += gcnew System::EventHandler(this, &Display::artistLabel_MouseEnter);
			this->artistLabel->MouseLeave += gcnew System::EventHandler(this, &Display::artistLabel_MouseLeave);
			// 
			// vanishingTimer
			// 
			this->vanishingTimer->Enabled = true;
			this->vanishingTimer->Tick += gcnew System::EventHandler(this, &Display::vanishingTimer_Tick);
			// 
			// mouseTimer
			// 
			this->mouseTimer->Enabled = true;
			this->mouseTimer->Tick += gcnew System::EventHandler(this, &Display::mouseTimer_Tick);
			// 
			// Display
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(284, 262);
			this->ControlBox = false;
			this->Controls->Add(this->artistLabel);
			this->Controls->Add(this->pausePicture);
			this->Controls->Add(this->playPicture);
			this->Controls->Add(this->playpausePicture);
			this->Controls->Add(this->nextPicture);
			this->Controls->Add(this->backPicture);
			this->Controls->Add(this->titleLabel);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::None;
			this->MaximizeBox = false;
			this->MinimizeBox = false;
			this->Name = L"Display";
			this->ShowIcon = false;
			this->ShowInTaskbar = false;
			this->SizeGripStyle = System::Windows::Forms::SizeGripStyle::Hide;
			this->StartPosition = System::Windows::Forms::FormStartPosition::Manual;
			this->Text = L"Display";
			this->TopMost = true;
			this->MouseEnter += gcnew System::EventHandler(this, &Display::Display_MouseEnter);
			this->MouseLeave += gcnew System::EventHandler(this, &Display::Display_MouseLeave);
			this->contextMenu->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->backPicture))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->nextPicture))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->playpausePicture))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->playPicture))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pausePicture))->EndInit();
			this->ResumeLayout(false);

		}
#pragma endregion
	private:
		System::Void trayIcon_MouseClick(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {
			if(e->Button == System::Windows::Forms::MouseButtons::Left) {
				Visible = !Visible;
				if(Visible) {
					ShowActions();
				}
			}
		}
	
		System::Void backPicture_MouseClick(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {
			broker->prevTrack();
		}
		
		System::Void playpausePicture_MouseClick(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {
			broker->playPause();
		}
		
		System::Void nextPicture_MouseClick(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {
			broker->nextTrack();
		}
		
		System::Void showWinampToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			broker->showWinamp();
		}

		System::Void exitWinampToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			broker->exitWinamp();
		}

		System::Void vanishingTimer_Tick(System::Object^  sender, System::EventArgs^  e) {
			if(Visible && !overSubcomponent) {
				if (lastOver >= MAX_SHOW_TIME) {
					Opacity -= VANISH_DECREMENT;
				}

				if(Opacity <= 0) {
					Visible = false;
				}				
			}
		}

		System::Void mouseTimer_Tick(System::Object^  sender, System::EventArgs^  e) {
			lastOver += mouseTimer->Interval;
		}

		System::Void Display_MouseEnter(System::Object^  sender, System::EventArgs^  e) {
			ShowActions();
			mouseTimer->Stop();
		}
		
		System::Void Display_MouseLeave(System::Object^  sender, System::EventArgs^  e) {
			ShowActions();
			mouseTimer->Start();
		}
		
		System::Void nextPicture_MouseEnter(System::Object^  sender, System::EventArgs^  e) {
			overSubcomponent = true;
			Display_MouseEnter(this, e);
		}
		
		System::Void playpausePicture_MouseEnter(System::Object^  sender, System::EventArgs^  e) {
			overSubcomponent = true;
			Display_MouseEnter(this, e);
		}
		
		System::Void backPicture_MouseEnter(System::Object^  sender, System::EventArgs^  e) {
			overSubcomponent = true;
			Display_MouseEnter(this, e);
		}

		System::Void titleLabel_MouseEnter(System::Object^  sender, System::EventArgs^  e) {
			overSubcomponent = true;
			Display_MouseEnter(this, e);
		}

		System::Void artistLabel_MouseEnter(System::Object^  sender, System::EventArgs^  e) {
			overSubcomponent = true;
			Display_MouseEnter(this, e);
		}

		System::Void playPicture_MouseEnter(System::Object^  sender, System::EventArgs^  e) {
			overSubcomponent = true;
			Display_MouseEnter(this, e);
		}

		System::Void pausePicture_MouseEnter(System::Object^  sender, System::EventArgs^  e) {
			overSubcomponent = true;
			Display_MouseEnter(this, e);
		}

		System::Void titleLabel_MouseLeave(System::Object^  sender, System::EventArgs^  e) {
			overSubcomponent = false;
		}

		System::Void artistLabel_MouseLeave(System::Object^  sender, System::EventArgs^  e) {
			overSubcomponent = false;
		}

		System::Void playpausePicture_MouseLeave(System::Object^  sender, System::EventArgs^  e) {
			overSubcomponent = false;
		}

		System::Void nextPicture_MouseLeave(System::Object^  sender, System::EventArgs^  e) {
			overSubcomponent = false;
		}

		System::Void backPicture_MouseLeave(System::Object^  sender, System::EventArgs^  e) {
			overSubcomponent = false;
		}

		System::Void pausePicture_MouseLeave(System::Object^  sender, System::EventArgs^  e) {
			overSubcomponent = false;
		}

		System::Void playPicture_MouseLeave(System::Object^  sender, System::EventArgs^  e) {
			overSubcomponent = false;
		}

};
}
