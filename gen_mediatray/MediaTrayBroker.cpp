#include "StdAfx.h"
#include "MediaTrayBroker.h"

MediaTrayBroker::MediaTrayBroker(winampGeneralPurposePlugin* plugin){
	this->plugin = plugin;
}

MediaTrayBroker::~MediaTrayBroker(void){
}

int MediaTrayBroker::init(void) {
	return 0;
}

void MediaTrayBroker::config(void) {
	DisplayMessage(L"MediaTray\r\nCopyright (C) 2011 - Jesse Bowes");
}

void MediaTrayBroker::quit(void) {
}

void MediaTrayBroker::DisplayMessage(LPCTSTR str){
	MessageBox(0, str, L"", MB_OK);
}

String^ MediaTrayBroker::getArtist() {
	wchar_t *filename = (wchar_t *)SendMessage(plugin->hwndParent,WM_WA_IPC,0,IPC_GET_PLAYING_FILENAME);
	
	if(filename && *filename)
	{
		wchar_t artist[1024];
		extendedFileInfoStructW a = {filename,L"artist",artist,1024};
		SendMessage(plugin->hwndParent,WM_WA_IPC,(LPARAM)&a,IPC_GET_EXTENDED_FILE_INFOW);
		return gcnew String(artist);
	}

	return "<Artist>";
}

String^ MediaTrayBroker::getTitle() {
	wchar_t *filename = (wchar_t *)SendMessage(plugin->hwndParent,WM_WA_IPC,0,IPC_GET_PLAYING_FILENAME);
	
	if(filename && *filename)
	{
		wchar_t title[1024];
		extendedFileInfoStructW a = {filename,L"title",title,1024};
		SendMessage(plugin->hwndParent,WM_WA_IPC,(LPARAM)&a,IPC_GET_EXTENDED_FILE_INFOW);
		return gcnew String(title);
	}

	return "<Title>";
}

void MediaTrayBroker::nextTrack() {
	if(isPaused()) {
		stopSong();
	}
	SendMessage(plugin->hwndParent,WM_COMMAND,WINAMP_BUTTON_NEXT,0);
}

void MediaTrayBroker::prevTrack() {
	if(isPaused()) {
		stopSong();
	}
	SendMessage(plugin->hwndParent,WM_COMMAND,WINAMP_BUTTON_PREV,0);
}

void MediaTrayBroker::playPause() {
	if(isPlaying()) {
		pauseSong();
	} else {
		playSong();
	}
}

void MediaTrayBroker::playSong() {
	SendMessage(plugin->hwndParent,WM_COMMAND,WINAMP_BUTTON_PLAY,0);
}

void MediaTrayBroker::stopSong() {
	SendMessage(plugin->hwndParent,WM_COMMAND,WINAMP_BUTTON_STOP,0);
}

void MediaTrayBroker::showWinamp() {
	ShowWindow(plugin->hwndParent, SW_MAXIMIZE);
}

void MediaTrayBroker::exitWinamp() {
	SendMessage(plugin->hwndParent,WM_COMMAND,WINAMP_FILE_QUIT,0);
}

bool MediaTrayBroker::isPlaying() {
	return (SendMessage(plugin->hwndParent,WM_WA_IPC,0,IPC_ISPLAYING) == 1);
}

bool MediaTrayBroker::isPaused() {
	return (SendMessage(plugin->hwndParent,WM_WA_IPC,0,IPC_ISPLAYING) == 3);
}

bool MediaTrayBroker::isStopped() {
	return (SendMessage(plugin->hwndParent,WM_WA_IPC,0,IPC_ISPLAYING) == 0);
}

void MediaTrayBroker::pauseSong() {
	SendMessage(plugin->hwndParent,WM_COMMAND,WINAMP_BUTTON_PAUSE,0);
}