#pragma once
#include "Winamp\GEN.H"
#include "Winamp\wa_ipc.h"
#include "Winamp\WINAMPCMD.H"
#include <windows.h>
#include <vcclr.h>
#include "MediaTrayBroker.h"


#define WINAMP_BUTTON_PREV WINAMP_BUTTON1
#define WINAMP_BUTTON_PLAY WINAMP_BUTTON2
#define WINAMP_BUTTON_PAUSE WINAMP_BUTTON3
#define WINAMP_BUTTON_STOP WINAMP_BUTTON4
#define WINAMP_BUTTON_NEXT WINAMP_BUTTON5

using namespace System;

ref class Popup;
class MediaTrayBroker
{
public:
	MediaTrayBroker(winampGeneralPurposePlugin*);
	~MediaTrayBroker(void);
	int init(void);
	void config(void);
	void quit(void);
	void DisplayMessage(LPCTSTR);
	String^ getArtist(void);
	String^ getTitle(void);
	void nextTrack(void);
	void prevTrack(void);
	void playPause(void);
	void playSong(void);
	void stopSong(void);
	void pauseSong(void);
	void showWinamp(void);
	void exitWinamp(void);
	bool isPlaying(void);
	bool isPaused(void);
	bool isStopped(void);
private:
	winampGeneralPurposePlugin* plugin;
};
