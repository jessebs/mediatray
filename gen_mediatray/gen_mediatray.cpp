// This is the main DLL file.

#include "stdafx.h"
#include "gen_mediatray.h"
#include <windows.h>

int init() {
	lpWndProcOld = (WNDPROC)SetWindowLong(plugin.hwndParent, GWL_WNDPROC, (LONG)MainWndProc);
	return broker->init();
}

void config() {
	SendMessage(plugin.hwndParent,WM_WA_IPC,0,WINAMP_FILE_QUIT); // DOESNT WORK BUT SHOULD
	broker->config();
}

void quit() {
	display->~Display();
	broker->quit();
}

// Winamp Subclassing
LRESULT CALLBACK MainWndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam) {
	if (message==WM_WA_IPC && lParam==IPC_CB_MISC && (wParam==IPC_CB_MISC_STATUS || wParam==IPC_CB_MISC_TITLE)){
		display->updateWinampData();
	}
	
 	return CallWindowProc(lpWndProcOld,hwnd,message,wParam,lParam);
}
