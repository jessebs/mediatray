// gen_mediatray.h

#pragma once

#include "Winamp\GEN.H"
#include "Display.h"
#include <windows.h>
#include <vcclr.h>
#include "MediaTrayBroker.h"

#define PLUGIN_NAME "MediaTray"

using namespace System;
using namespace gen_mediatray;

WNDPROC lpWndProcOld = NULL;

int init();
void config();
void quit();
winampGeneralPurposePlugin plugin = {GPPHDR_VER, PLUGIN_NAME, init, config, quit, 0, 0};
LRESULT CALLBACK MainWndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);
MediaTrayBroker* broker = new MediaTrayBroker(&plugin);
gcroot<Display^> display = gcnew Display(broker);


extern "C" __declspec(dllexport) winampGeneralPurposePlugin * winampGetGeneralPurposePlugin() {return &plugin;}
